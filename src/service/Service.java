package service;
import org.testng.annotations.Test;
import main.Main;
import pageObjects.ForPacientPageLuxmedWeb_Component;
import pageObjects.MainPageLuxmedWeb_Component;
import pageObjects.ScheduleDocListPageLuxmedWeb_Component;
import pageObjects.ScheduleDocPageluxmedWeb_Component;
import static utils.Methods.*;
import java.io.IOException;
public class Service extends Main{
	@Test
	public void scheduleDoctor() throws InterruptedException, IOException {
		MainPageLuxmedWeb_Component main = new MainPageLuxmedWeb_Component(driver);
		driver.manage().window().maximize();
		main.openPage();
		main.selectForPatient();
		//Thread.sleep(5000);
		ForPacientPageLuxmedWeb_Component pacient = new ForPacientPageLuxmedWeb_Component(driver);
		pacient.selectScheduleDoc();
		Thread.sleep(3000);
		ScheduleDocPageluxmedWeb_Component sche = new ScheduleDocPageluxmedWeb_Component(driver);
		sche.selectCity(parameters.getParameter("miasto"));
		sche.selectCenter(parameters.getParameter("placówka"));
		sche.selectSpec(parameters.getParameter("specjalizacja"));
		sche.selectDoc(parameters.getParameter("doktor"));
		sche.getSchedule();
		Thread.sleep(3000);
		ScheduleDocListPageLuxmedWeb_Component list = new ScheduleDocListPageLuxmedWeb_Component(driver);
		Thread.sleep(3000);
		list.showSchedule();
		//Thread.sleep(15000);
	}
}
